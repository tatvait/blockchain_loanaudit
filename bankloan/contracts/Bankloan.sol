pragma solidity ^0.5.0;

contract bankloan {
	// Model a Candidate
	struct Loan {
		uint loanTaken;
		uint loanTenure; // in months 
		uint interestRate;
		// Calucate EMI
		uint loanPaidBack;
		uint loanOutStanding;
	}

	// Constructor
	constructor() public {
		}

	// Read/write candidates
	mapping(address => Loan) public loanData;
	
	function payBack (uint _amount) public {
		Loan memory curLoanAcc = loanData[msg.sender];
		loanData[msg.sender].loanOutStanding = curLoanAcc.loanOutStanding - _amount;
		loanData[msg.sender].loanPaidBack = curLoanAcc.loanPaidBack + _amount;
	}

	function borrow(uint _amount) public {
		loanData[msg.sender] = Loan(_amount, 120, 0, 0, _amount);
	}
}