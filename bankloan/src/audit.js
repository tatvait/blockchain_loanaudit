App = {
	web3Provider: null,
	//contracts: {},
	//account: '0x0',
	init: function() {
		return App.initWeb3();
	},
	initWeb3: function() {
		if (typeof web3 !== 'undefined') {
			console.log("type of web3: " + typeof web3);
			// If a web3 instance is already provided by Meta Mask.
			App.web3Provider = web3.currentProvider;
			web3 = new Web3(web3.currentProvider);
			console.log("current web3");
		} else {
			// Specify default instance if no web3 instance provided
			App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
			web3 = new Web3(App.web3Provider);
			console.log("new web3");
		}
		App.initContract();
	},
	initContract: function() {
		$.getJSON("bankloan.json", function(bankloan) {
			// Instantiate a new truffle contract from the artifact
			App.Bankloan = TruffleContract(bankloan);
			// Connect provider to interact with contract
			App.Bankloan.setProvider(App.web3Provider);
		});
	},
	audit: function() {
		var addr = $("#addr").val();
		addr = new String(addr).toLowerCase();
		console.log(addr);
		var trxData = $("#TransactionData");
		trxData.empty();

		web3.eth.getBlockNumber(function(err, bnum) {
			console.log(bnum);
			for(i=1; i<= bnum; i++) {
				var idx = 0;
				web3.eth.getBlock(i, true, function(err, b) {
					var from = new String(b.transactions[0].from).toLowerCase();
					console.log(from);
					console.log(addr);
					if(from === addr) {
						console.log("found a match.");
						idx++;
						var hash = b.transactions[0].hash;
						var inpData = b.transactions[0].input;
						console.log(hash);
						var trxDisplayData = "<tr><th>" + idx + "</th><td>" + hash + "</td><td>" + inpData + "</td></tr>";
						trxData.append(trxDisplayData);
					}
				}); 
			}
		});	
	},
	borrow: function() {
		var amount = $("#borrowAmount").val();
		console.log(amount);
		var bankloanInstance;
		// Load account data
		web3.eth.getCoinbase(function(err, account) {
			if (err === null) {
				console.log("account: "+ account);
				App.account = account;
				$("#accountAddress").html("Your Account: " + account);

				App.Bankloan.deployed().then(function(instance) {
					bankloanInstance = instance;
					bankloanInstance.borrow(amount);
					alert(App.account + " is borrowing: " + amount);
				});
			}
		});
	},
	payback: function() {
		var amount = $("#paybackAmount").val();
		console.log(amount);
		var bankloanInstance;
		// Load account data
		web3.eth.getCoinbase(function(err, account) {
			if (err === null) {
				console.log("account: "+ account);
				App.account = account;
				$("#accountAddress").html("Your Account: " + account);

				App.Bankloan.deployed().then(function(instance) {
					bankloanInstance = instance;
					bankloanInstance.payBack(amount);
					alert(App.account + " is paying back: " + amount);
				});
			}
		});
	}

};

App.init();
